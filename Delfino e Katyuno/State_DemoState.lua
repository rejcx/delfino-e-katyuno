DemoState = Core.class()

function DemoState:init( options )
	self.name = options.name	
end

function DemoState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/InGame/background_sky.png" ),
		waves = Texture.new( "Graphics/InGame/tile_wave.png" ),
		waves2 = Texture.new( "Graphics/InGame/tile_wave2.png" ),
	}
	
	self.tileTextures = {
		tileHeart = { texture = Texture.new( "Graphics/InGame/trinket_heart.png" ), name = "heart" },
		tileEnd = { texture = Texture.new( "Graphics/InGame/tile_ending.png" ), name = "ending" },
		
		-- Happy things
		-- { texture = Texture.new( "Graphics/InGame/trinket_tea.png" ), name = "tea" },
		tileTea = 		{ texture = Texture.new( "Graphics/InGame/trinket_tea.png" ), 		name = "tea" },
		tileBell = 		{ texture = Texture.new( "Graphics/InGame/trinket_bell.png" ), 		name = "bell" },
		tileFish = 		{ texture = Texture.new( "Graphics/InGame/trinket_fish.png" ), 		name = "fish" },
		tileLotus = 	{ texture = Texture.new( "Graphics/InGame/trinket_lotus.png" ), 	name = "lotus" },
		tileBalloon = 	{ texture = Texture.new( "Graphics/InGame/trinket_balloon.png" ), 	name = "balloon" },
		tileAcorn = 	{ texture = Texture.new( "Graphics/InGame/trinket_acorn.png" ), 	name = "acorn" },
		tileFeather = 	{ texture = Texture.new( "Graphics/InGame/trinket_feather.png" ), 	name = "feather" },
		tileCandy = 	{ texture = Texture.new( "Graphics/InGame/trinket_candy.png" ), 	name = "candy" },
		tileFrog = 		{ texture = Texture.new( "Graphics/InGame/trinket_frog.png" ), 		name = "frog" },
		tilePenguin = 	{ texture = Texture.new( "Graphics/InGame/trinket_penguin.png" ), 	name = "penguin" },
		tileSun = 		{ texture = Texture.new( "Graphics/InGame/trinket_sun.png" ), 		name = "sun" },
		tileButterfly = { texture = Texture.new( "Graphics/InGame/trinket_butterfly.png" ), name = "butterfly" },
		
		-- Bad things
		tileSmallRock = { texture = Texture.new( "Graphics/InGame/obstacle_rock1.png" ), 	name = "smallrock" },
		tileBigRock = 	{ texture = Texture.new( "Graphics/InGame/obstacle_rock2.png" ), 	name = "bigrock" },
		tileHawk = 		{ texture = Texture.new( "Graphics/InGame/obstacle_hawk.png" ), 	name = "hawk" },
		tileShark = 	{ texture = Texture.new( "Graphics/InGame/obstacle_shark.png" ), 	name = "shark" },
		tileBomb = 		{ texture = Texture.new( "Graphics/InGame/obstacle_bomb.png" ), 	name = "bomb" },
		tileEgret = 	{ texture = Texture.new( "Graphics/InGame/obstacle_egret.png" ), 	name = "egret" },
		tileHippo = 	{ texture = Texture.new( "Graphics/InGame/obstacle_hippo.png" ), 	name = "hippo" },
		tileOctopus = 	{ texture = Texture.new( "Graphics/InGame/obstacle_octopus.png" ), 	name = "octopus" },
		tileOwl = 		{ texture = Texture.new( "Graphics/InGame/obstacle_owl.png" ), 		name = "owl" },
		tileSnake = 	{ texture = Texture.new( "Graphics/InGame/obstacle_snake.png" ), 	name = "snake" },
	}
		
	self.background = Bitmap.new( self.textures.background )
	
	self.images = {}
	
	self.waveCount1 = 12
	for i = 1, self.waveCount1 do
		self.images[i] = Bitmap.new( self.textures.waves )
	end
	
	self.waveCount2 = 23
	for i = self.waveCount1+1, self.waveCount2 do
		self.images[i] = Bitmap.new( self.textures.waves2 )
	end
	
	self.dolphin = DolphinEntity.new()
	self.dolphin:Setup()
	
	self.cat = CatEntity.new()
	self.cat:Setup()
	
	self.scrollSpeed = 1
	self.scrollAmount = 1000
	self.lastAction = 0
	
	self.score = 0
	self.scoreLabel = TextField.new( fontManager.main, self.score )
	self.scoreLabel:setPosition( 10, 20 )
	
	self.jumpMarkerTimerMax = 15
	self.jumpMarkerTimer = 0
	self.jumpMarkerX = 0
	self.jumpMarkerY = 0
	
	-- For the pause/gameover screen
	self.labels = {
		status = TextField.new( fontManager.header, languageManager:GetString( "YouLose" ) ),
		retry = TextField.new( fontManager.header, languageManager:GetString( "RetryLevel" ) ),
		levelselect = TextField.new( fontManager.header, languageManager:GetString( "LevelSelect" ) ),
		nextlevel = TextField.new( fontManager.header, languageManager:GetString( "NextLevel" ) ),
	}
	
	self.gameTips = {}
	
	-- Load level
	local level = "demo"
	print( level )
	
	if EditorOptionsState.runTest == true then
		level = "test"
		
	elseif CustomLoaderState.runCustom == true then
		level = CustomLoaderState.loadLevel
		
	end
	
	self:SetupLevel( level )
	
	self.endLevelScreen = false
	
	self.state = "play" -- play, fail, win, pause
	
	local ct = 0
	for k, v in pairs( self.trinkets ) do
		ct = ct + 1
	end
	
	self.effects = {}
	
	self.dolphin:AddExtraLife()
	
	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	self.dolphin.bitmap:addEventListener( Event.MOUSE_DOWN, self.Event_DolphinPoke, self )
	self.cat.bitmap:addEventListener( Event.MOUSE_DOWN, self.Event_CatPoke, self )
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
	self.background:addEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
end

function DemoState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.dolphin:Clear()
	self.cat:Clear()
	
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )	
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
	self.background:removeEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
end

function DemoState:Event_AndroidKey( event )
	if event.keyCode == 301 then -- Back button
	
		if EditorOptionsState.runTest == true then
			StateManager:StateSetup( "editoroptions" )

		elseif self.state == "play" then
			self:CreatePauseMenu( "pause" ) -- fail, win, pause
			
		elseif self.state == "pause" then
			self:ClearPauseMenu()
			
		elseif self.state == "fail" or self.state == "win" then
			if EditorOptionsState.runTest == true then
				StateManager:StateSetup( "editoroptions" )
			else
				StateManager:StateSetup( "levelselect" )
			end

		end
	end
end

function DemoState:ReloadLanguage()
	self.labels.retry:setText( languageManager:GetString( "RetryLevel" ) )
	
	if EditorOptionsState.runTest == true then
		self.labels.levelselect:setText( languageManager:GetString( "BackToEditor" ) )
	else
		self.labels.levelselect:setText( languageManager:GetString( "LevelSelect" ) )
	end
	
	self.labels.nextlevel:setText( languageManager:GetString( "NextLevel" ) )
end

function DemoState:CreatePauseMenu( type )

	if self.state ~= "play" then
		return
	end

	self.state = type
	
	--self.labels.status = TextField.new( fontManager.header, languageManager:GetString( "YouLose" ) ),
	--self.labels.retry = TextField.new( fontManager.header, languageManager:GetString( "RetryLevel" ) ),
	--self.labels.levelselect = TextField.new( fontManager.header, languageManager:GetString( "LevelSelect" ) ),
	--self.labels.nextlevel = TextField.new( fontManager.header, languageManager:GetString( "NextLevel" ) ),
	
	self.labelPaused = TextField.new( fontManager.header, languageManager:GetString( "PausedGame" ) )
	self.labelPaused:setPosition( 20, 30 )
	self.labelPaused:setTextColor( 0xffffff )
	
	if type == "win" then
		self.labelPaused:setText( languageManager:GetString( "YouWin" ) )
		xpos = SCREEN_WIDTH / 2 - fontManager.headerWidth * string.len( languageManager:GetString( "YouWin" ) ) / 2
		self.labelPaused:setTextColor( 0x00ff00 )
		self.labelPaused:setPosition( xpos, 30 )
	
	elseif type == "fail" then
		self.labelPaused:setText( languageManager:GetString( "YouLose" ) )
		xpos = SCREEN_WIDTH / 2 - fontManager.headerWidth * string.len( languageManager:GetString( "YouLose" ) ) / 2
		self.labelPaused:setTextColor( 0xff6666 )
		self.labelPaused:setPosition( xpos, 30 )
		
	end

	self.labelLevel = TextField.new( fontManager.main, languageManager:GetString( "Level" ) .. " " .. StateManager.gotoLevel )
	if CustomLoaderState.runCustom == true then
		self.labelLevel:setText( languageManager:GetString( "Level" ) .. " " .. CustomLoaderState.loadLevel )
		
	elseif EditorOptionsState.runTest == true then
		self.labelLevel:setText( languageManager:GetString( "Level" ) .. " test" )
	
	end

	self.retryBackground = Bitmap.new( Texture.new( "Graphics/UserInterface/background_dim.png" ) )

	self.buttons = {
		retry = Bitmap.new( Texture.new( "Graphics/UserInterface/button_retry.png" ) ),
		levelselect = Bitmap.new( Texture.new( "Graphics/UserInterface/button_levelselect.png" ) ),
		nextlevel = Bitmap.new( Texture.new( "Graphics/UserInterface/button_nextlevel.png" ) ),
	}
	
	if EditorOptionsState.runTest == true then
		self.labels.levelselect:setText( languageManager:GetString( "BackToEditor" ) )
		
	end
	
	-- Setup positions	
	self.labelLevel:setPosition( 20, 50 )
	self.labelLevel:setTextColor( 0xffffff )
	
	local btnHeight = 60
	local y1 = SCREEN_HEIGHT - (btnHeight * 3) - 30
	local y2 = SCREEN_HEIGHT - (btnHeight * 2) - 20
	local y3 = SCREEN_HEIGHT - btnHeight - 10
	
	self.buttons.nextlevel:setPosition( 10, y1 )
	self.buttons.levelselect:setPosition( 10, y2 )
	self.buttons.retry:setPosition( 10, y3 )
	
	self.labels.nextlevel:setTextColor( 0xffffff )
	self.labels.nextlevel:setPosition( 80, y1 + 40 )
	
	self.labels.levelselect:setTextColor( 0xffffff )
	self.labels.levelselect:setPosition( 80, y2 + 40 )
	
	self.labels.retry:setTextColor( 0xffffff )
	self.labels.retry:setPosition( 80, y3 + 40 )
	
	
	-- Draw items
	stage:addChild( self.retryBackground )
	stage:addChild( self.labelPaused )
	stage:addChild( self.labelLevel )
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
	
	if EditorOptionsState.runTest == true or CustomLoaderState.runCustom == true or StateManager.gotoLevel == 5 then
		stage:addChild( self.labels.levelselect )	
		stage:addChild( self.labels.retry )
		
		stage:addChild( self.buttons.levelselect )	
		stage:addChild( self.buttons.retry )
		
		if stage:contains( self.buttons.nextlevel ) then stage:removeChild( self.buttons.nextlevel ) end
		if stage:contains( self.labels.nextlevel ) then stage:removeChild( self.labels.nextlevel ) end
		
	else
		stage:addChild( self.labels.nextlevel )	
		stage:addChild( self.labels.levelselect )	
		stage:addChild( self.labels.retry )
		
		stage:addChild( self.buttons.nextlevel )	
		stage:addChild( self.buttons.levelselect )	
		stage:addChild( self.buttons.retry )
		
	end
end

function DemoState:ClearPauseMenu()
	self.state = "play" -- play, fail, win, pause
	
	if stage:contains( self.retryBackground ) then stage:removeChild( self.retryBackground ) end			
	if stage:contains( self.labelPaused ) then stage:removeChild( self.labelPaused ) end	
	if stage:contains( self.labelLevel ) then stage:removeChild( self.labelLevel ) end	

	for key, value in pairs( self.buttons ) do
		if stage:contains( value ) then stage:removeChild( value ) end
	end		
	
	for key, value in pairs( self.labels ) do
		if stage:contains( value ) then stage:removeChild( value ) end
	end
end

function DemoState:Event_MouseClick( event )

	if self.buttons == nil then
		return
	end

	if self.buttons.retry:hitTestPoint( event.x, event.y ) then
		if self.state ~= "play" then
			StateManager:StateSetup( "storygame" )
		end
	
	elseif self.buttons.levelselect:hitTestPoint( event.x, event.y ) then
		
		if self.state ~= "play" then
			
			if EditorOptionsState.runTest == true then
				StateManager:StateSetup( "editoroptions" )
			
			elseif CustomLoaderState.runCustom == true then
				StateManager:StateSetup( "customloader" )
			
			else
				StateManager:StateSetup( "levelselect" )
				
			end
		
		end
	
	
	elseif self.buttons.nextlevel:hitTestPoint( event.x, event.y ) then
		
		if self.state ~= "play" then
			
			if EditorOptionsState.runTest == true then
				StateManager:StateSetup( "editoroptions" )
			
			else
				StateManager.gotoLevel = StateManager.gotoLevel + 1
				StateManager:StateSetup( "storygame" )
				
			end
		
		end
		
	end
	
end

function DemoState:Update()
	if self.state == "play" then
		self.scrollAmount = self.scrollAmount + self.scrollSpeed
		
		if self.scrollAmount == 4000 then
			StateManager:StateSetup( "demo" )		
		end
		
		if self.lastAction == 0 then
			if self.scrollAmount % 300 == 0 then
			audioManager.dolphinjump:play()
			self.dolphin:Jump()
			end
			
			if self.scrollAmount % 150 == 0 then
			audioManager.dolphinjump:play()
			self.cat:Jump()
			end
		end

		-- Move waves
		for i = 1, self.waveCount1 do
			local index = i - 1
			local width = 48
			local scrollAdjust = self.scrollAmount * 10
			self.images[i]:setPosition( index * width - ( scrollAdjust % 80 ), SCREEN_HEIGHT-50 )
		end
		for i = self.waveCount1+1, self.waveCount2 do
			local index = (i - 1) - self.waveCount1
			local width = 60
			local scrollAdjust = self.scrollAmount * 10
			self.images[i]:setPosition( index * width - ( scrollAdjust * 1.25 % 100 ), SCREEN_HEIGHT-15 )
		end
		
		for key, tip in pairs( self.gameTips ) do 
			local text = tip:getText()
			local x, y = tip:getPosition()
			x = x - 3
			tip:setPosition( x, y )
			
			if x <= SCREEN_WIDTH + 300 then
				if stage:contains( tip ) == false then stage:addChild( tip ) end
			
			elseif x < -300 then
				if stage:contains( tip ) then stage:removeChild( tip ) end
			end
		end
		
		-- Collision
		for key, trinket in pairs( self.trinkets ) do
			trinket:Update( self.scrollAmount )
			
			-- Hit end level flag?
			if trinket:EndLevelHit( self.dolphin.position.x ) then
				self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )	
				--StateManager:StateSetup( "levelselect", true )
				
				self:CreatePauseMenu( "win" ) -- fail, win, pause
			end
		
			-- Cat Collision
			if trinket.active == true and self.cat:IsDead() == false and self:IsCollision( trinket, self.cat:GetCollisionRegion() ) then

				if trinket.affect.score ~= nil then
					self.score = self.score + trinket.affect.score
				end
				
				-- GOOD TRINKET
				if trinket.affect.trinket == true then	
						audioManager.getbell:play()

				-- BAD OBSTACLE
				elseif trinket.affect.damage == true then
					audioManager.cancel:play()
					
					if self.dolphin:ExtraLifeCount() > 0 then
						self.dolphin:RemoveExtraLife()
						if stage:contains( self.dolphin.heart ) then stage:removeChild( self.dolphin.heart ) end
					else
						self.cat:Lose()
						
						self:CreatePauseMenu( "fail" ) -- fail, win, pause
					end
				
				-- GOOD EXTRA LIFE
				elseif trinket.affect.life == true then
					audioManager.getbell:play()
					
					if self.dolphin:ExtraLifeCount() <= 0 then
						self.dolphin:AddExtraLife()
					else
						self.score = self.score + 10
					end
					
					self.dolphin.lives = 1
				
				end	
				
				trinket:Deactivate()
			end
			
			-- Dolphin Collision
			if trinket.active == true and self.cat:IsDead() == false and self:IsCollision( trinket, self.dolphin:GetCollisionRegion() ) then
				if trinket.type == "shark" then
					trinket:Deactivate()
					audioManager.cancel:play()
					
					if self.dolphin:ExtraLifeCount() > 0 then
						self.dolphin:RemoveExtraLife()
						if stage:contains( self.dolphin.heart ) then stage:removeChild( self.dolphin.heart ) end
					else
						self.cat:Lose()
						self.dolphin:Lose()
						self:CreatePauseMenu( "fail" ) -- fail, win, pause
					end
				
				elseif trinket.type == "bigrock" then
					trinket:Deactivate()
					audioManager.cancel:play()
					
					if self.dolphin:ExtraLifeCount() > 0 then
						self.dolphin:RemoveExtraLife()
						if stage:contains( self.dolphin.heart ) then stage:removeChild( self.dolphin.heart ) end
					else
						self.cat:Lose()
						self.dolphin:Lose()
						self:CreatePauseMenu( "fail" ) -- fail, win, pause
					end
				
				elseif trinket.type == "smallrock" then
					trinket:Deactivate()
					audioManager.cancel:play()
					
					if self.dolphin:ExtraLifeCount() > 0 then
						self.dolphin:RemoveExtraLife()
						if stage:contains( self.dolphin.heart ) then stage:removeChild( self.dolphin.heart ) end
					else
						self.cat:Lose()
						self.dolphin:Lose()
						self:CreatePauseMenu( "fail" ) -- fail, win, pause
					end
				
				elseif trinket.type == "fish" then
					trinket:Deactivate()
					self.score = self.score + 5
					audioManager.getbell:play()
					
					
				-- GOOD EXTRA LIFE
				elseif trinket.affect.life == true then
					audioManager.getbell:play()
					
					if self.dolphin:ExtraLifeCount() <= 0 then
						self.dolphin:AddExtraLife()
					else
						self.score = self.score + 10
					end
					
					self.dolphin.lives = 1
					trinket:Deactivate()
				
				
				end
			end
		end
		
		self.cat:IsFalling( self.dolphin )
		self.dolphin:IsFalling( self.dolphin )
		
		self.scoreLabel:setText( self.score )
	
	end
	
	local deleteKeys = {}
	for key, value in pairs( self.effects ) do
		value:Update()
		
		if value.destroy == true then
			table.insert( deleteKeys, key )
		end
	end
	
	for key, value in pairs( deleteKeys ) do 
		table.remove( self.effects, value )
	end
	
end

function DemoState:Draw()	
	stage:addChild( self.background )
	
	for i = 1, self.waveCount1 do
		stage:addChild( self.images[i] )
	end
	
	for key, trinket in pairs( self.trinkets ) do
		trinket:Draw()
	end
	
	self.dolphin:Draw()
	self.cat:Draw()
	
	for i = self.waveCount1+1, self.waveCount2 do
		stage:addChild( self.images[i] )
	end
	
	stage:addChild( self.scoreLabel )
end

function DemoState:Clear()	
	stage:removeChild( self.background )
	
	for key, trinket in pairs( self.trinkets ) do
		trinket:Clear()
	end
	
	for key, value in pairs( self.images ) do
		stage:removeChild( value )
	end
	  
	self.dolphin:Clear()
	stage:removeChild( self.scoreLabel )
	
	if self.retryBackground ~= nil then
		if stage:contains( self.retryBackground ) then stage:removeChild( self.retryBackground ) end
	end
	
	if self.buttons ~= nil then 
		for key, value in pairs( self.buttons ) do
			if stage:contains( value ) then stage:addChild( value ) end
		end
	end
	
	for key, value in pairs( self.labels ) do
		if stage:contains( value ) then stage:addChild( value ) end
	end
end

function DemoState:Event_DolphinPoke( event )
	self.lastAction = self.scrollAmount
	
	if self.state == "play" and self.cat:IsDead() == false and self.dolphin.bitmap:hitTestPoint( event.x, event.y ) then
		if self.cat.falling == false then
			-- First Jump
			self.cat:Jump()
			self.dolphin:Launch()
			audioManager.catjump:play()
		
			local markerEffect = JumpMarkerSfx.new()
			markerEffect:Setup( event.x, event.y )
			table.insert( self.effects, markerEffect )
		else
			-- Dolphin jump
			audioManager.dolphinjump:play()
			self.dolphin:Jump()
		
			local markerEffect = JumpMarkerSfx.new()
			markerEffect:Setup( event.x, event.y )
			table.insert( self.effects, markerEffect )
		end
	end
end

function DemoState:Event_CatPoke( event )
	self.lastAction = self.scrollAmount
	
	if self.state == "play" and self.cat:IsDead() == false then
		-- self.cat.bitmap:hitTestPoint( event.x, event.y )
		if 	event.x > self.cat.position.x - 20 and
			event.y > self.cat.position.y - 10 and
			event.x < self.cat.position.x + self.cat.dimensions.width + 10 and
			event.y < self.cat.position.y + self.cat.dimensions.height + 10 then
			self.cat:Jump()
			self.dolphin:Launch()
			audioManager.catjump:play()
			
			local markerEffect = JumpMarkerSfx.new()
			markerEffect:Setup( event.x, event.y )
			table.insert( self.effects, markerEffect )
		end
	end
end

function DemoState:IsCollision( object1, object2 )
	return  
		( object1.position.x < object2.position.x + object2.dimensions.width and
		object1.position.x + object1.dimensions.width > object2.position.x and
		object1.position.y < object2.position.y + object2.dimensions.height and
		object1.position.y + object1.dimensions.height > object2.position.y )
end

function DemoState:SetupLevel( levelname )
	self.trinkets = {}
	
	local prefix = "|R|Levels/"
	local filename = prefix .. levelname .. ".maro"
	
	if EditorOptionsState.runTest == true or CustomLoaderState.runCustom == true then
		filename = "/sdcard/Delfino/" .. levelname .. ".maro"
	end
		
	local sourceFile = io.open( filename, "rb" )
	
	if sourceFile == nil then
		if EditorOptionsState.runTest == true or CustomLoaderState.runCustom == true then
			prefix = "|D|"
			filename = prefix .. levelname .. ".maro"
			sourceFile = io.open( filename, "rb" )
		end
	end
	
	if sourceFile == nil then	
		local alertDialog = AlertDialog.new( languageManager:GetString( "IMessedUp" ), languageManager:GetString( "UnableToOpenLevel" ) .. " '" .. filename .. "'. " .. languageManager:GetString( "PleaseContact" ) .. " " .. languageManager:GetString( "EMAIL_ADDRESS" ), languageManager:GetString( "Damnit" ) )
		alertDialog:show()
	else					
		for line in sourceFile:lines() do	
			-- Find whitespace
			local whitespaceIndices = {}
			local count = 0
			for i = 1, string.len( line ) do 				
				local substr = string.sub( line, i, i )				
				if substr == " " then
					table.insert( whitespaceIndices, i )
					count = count + 1
				end	
			end		
			count = count + 1
			whitespaceIndices[ count ] = string.len( line ) + 1
			
			-- Read each command
			local localx, localy, tileType, tipText, localmov = nil, nil, nil, nil, nil
			local position = 1
			local lastCommand = nil
			for i = 1, count do
				
				local substr = string.sub( line, position, whitespaceIndices[i]-1 )
				position = whitespaceIndices[i] + 1
				
				if lastCommand == "x" then
					localx = substr
				
				elseif lastCommand == "y" then
					localy = substr
				
				elseif lastCommand == "type" then
					tileType = substr
					
				elseif lastCommand == "movement" then
					localmov = substr
					
				elseif lastCommand == "text" then
					-- Get the text key
					tipText = substr
					tileType = "text"
					
				elseif substr == "end" then
					-- Save
					
					local trinketTexture = nil
					-- Figure out texture
					for key, value in pairs( self.tileTextures ) do
						if value.name == tileType then
							trinketTexture = value.texture
						end
					end
					
					if tileType == "text" then
						local x = tonumber( localx ) * 40 --+ self.scrollAmount
						local y = tonumber( localy ) * 40
						local tip = TextField.new( fontManager.main, languageManager:GetString( tipText ) )
						tip:setPosition( x, y )
						tip:setTextColor( 0x134c91 )

						table.insert( self.gameTips, tip )
					
					elseif trinketTexture ~= nil then
						local trinket = Entity.new()
						local locx = tonumber( localx ) * 40 + self.scrollAmount
						local locy = tonumber( localy ) * 40
						trinket:Setup( { type = tileType, texture = trinketTexture, x = locx, y = locy, width = 40, height = 40, movement = localmov } )		
						table.insert( self.trinkets, trinket )
					
					end
				
				end
				
				lastCommand = substr
			end
			
		end
		
		sourceFile:close()
	
	end
	
	self:ReloadLanguage()
	
	local count = 0
	for key, value in pairs( self.trinkets ) do
		count = count + 1
	end
end

