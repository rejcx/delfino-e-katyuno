CustomLoaderState = Core.class()

function CustomLoaderState:init( options )
	self.name = options.name	
end

function CustomLoaderState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_checker.png" ),
		buttonbg = Texture.new( "Graphics/UserInterface/button_bg.png" ),
		buttonback = Texture.new( "Graphics/UserInterface/button_back.png" ),
		buttonforward = Texture.new( "Graphics/UserInterface/button_forward.png" ),
	}
	
	self.background = Bitmap.new( self.textures.background )
	
	self.buttons = {
		back = Bitmap.new( self.textures.buttonback ),
		forward = Bitmap.new( self.textures.buttonforward ),
	}

	self.buttons.back:setPosition( 0, SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	self.buttons.forward:setPosition( SCREEN_WIDTH - self.textures.buttonforward:getWidth(), SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	
	self.labels = {}
	--story = TextField.new( fontManager.button, languageManager:GetString( "Story" ) ),
	--self.labels.story:setTextColor( 0xffffff )
	
	self.labelButtons = {}
	
	require "lfs"
	
	iter, dir_obj = lfs.dir( [[/sdcard/Delfino/]] )
	local file = iter( dir_obj )
	if file == nil then
		iter, dir_obj = lfs.dir( [[|D|]] )
		file = iter( dir_obj )
	end

	self.fileList = {}
	self.fileCount = 0
	while file ~= nil do
		-- Only interested in ".maro" files.
		
		local length = string.len( file )
		if ( string.sub( file, length-4, length ) == ".maro" ) then
			table.insert( self.fileList, file )
			self.fileCount = self.fileCount + 1
		end
		
		if dir_obj:next() then
			file = iter( dir_obj )
		
		else 
			break
			
		end
	end
	
	dir_obj:close()
		
	self.scrollAmount = 0
	self.labelButtons[1] = { label = TextField.new( fontManager.button, "..." ), file = "" }
	self.labelButtons[2] = { label = TextField.new( fontManager.button, "..." ), file = "" }
	self.labelButtons[3] = { label = TextField.new( fontManager.button, "..." ), file = "" }
	self.labelButtons[4] = { label = TextField.new( fontManager.button, "..." ), file = "" }
	self.labelButtons[5] = { label = TextField.new( fontManager.button, "..." ), file = "" }
	
	local y, inc = 80, 40
	for k, v in pairs( self.labelButtons ) do
		v.label:setTextColor( 0xffffff )
		v.label:setPosition( 10, y )
		y = y + inc
	end
	
	
	if CustomLoaderState.runCustom == true then
		CustomLoaderState.runCustom = false
	end
	
	self:SetupButtons()
	
	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:addEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function CustomLoaderState:SetupButtons()
	local buttonCount = 1
	local fileCount = 0
	
	for key, value in pairs( self.labelButtons ) do
		value.label:setText( "..." )
		value.file = ""
		
	end
	
	for k, v in pairs( self.fileList ) do
		fileCount = fileCount + 1
		if fileCount >= self.scrollAmount * 5 + 1 and fileCount <= ( self.scrollAmount + 1 ) * 5 then
				
			if buttonCount < 6 then
				self.labelButtons[buttonCount].label:setText( fileCount .. ": " .. v )
				self.labelButtons[buttonCount].file = v
				buttonCount = buttonCount + 1
				
			end
		end
		
	end

	self.labels.count = TextField.new( fontManager.button, fileCount .. " total maps" )
	self.labels.count:setTextColor( 0xffffff )
	self.labels.count:setPosition( 10, 20 )
	
end

function CustomLoaderState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:removeEventListener( Event.MOUSE_DOWN, self.Event_MouseClick, self )
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function CustomLoaderState:Event_AndroidKey( event )
	if event.keyCode == 301 then
		-- back
		StateManager:StateSetup( "playmenu" )
	end
end

function CustomLoaderState:ReloadLanguage()
	--self.labels.story:setText( languageManager:GetString( "Story" ) )
end

function CustomLoaderState:Update()
end

function CustomLoaderState:Draw()
	stage:addChild( self.background )
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labelButtons ) do
		stage:addChild( value.label )
	end
end

function CustomLoaderState:Clear()
	if stage:contains( self.background ) then stage:removeChild( self.background ) end
	
	for key, value in pairs( self.buttons ) do
		if stage:contains( value ) then stage:removeChild( value ) end
	end
	
	for key, value in pairs( self.labels ) do
		if stage:contains( value ) then stage:removeChild( value ) end
	end
	
	for key, value in pairs( self.labelButtons ) do
		if stage:contains( value.label ) then stage:addChild( value.label ) end
	end
end

function CustomLoaderState:Event_MouseClick( event )	

	if self.buttons.back:hitTestPoint( event.x, event.y ) then
		self.scrollAmount = self.scrollAmount - 1
		if self.scrollAmount < 0 then self.scrollAmount = 0 end
		self:SetupButtons()
	
	elseif self.buttons.forward:hitTestPoint( event.x, event.y ) then
		self.scrollAmount = self.scrollAmount + 1
		if self.scrollAmount > math.floor( self.fileCount / 5 ) then
			self.scrollAmount = math.floor( self.fileCount / 5 )
		end
		self:SetupButtons()
	
	end
	
	for key, value in pairs( self.labelButtons ) do
		if value.label:hitTestPoint( event.x, event.y ) then
			-- Go to this specific level
			CustomLoaderState.runCustom = true
			local len = string.len( value.file )
			CustomLoaderState.loadLevel = string.sub( value.file, 1, len-5 )
			
			StateManager:StateSetup( "storygame", false )			
		end
	end
end
