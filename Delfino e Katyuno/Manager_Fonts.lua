FontManager = Core.class()

function FontManager:init()
	self.header 	= Font.new( "Fonts/NotCourierSans_40.txt", "Fonts/NotCourierSans_40.png" )
	self.main 		= Font.new( "Fonts/FantasqueSansMono-Bold_20.txt", "Fonts/FantasqueSansMono-Bold_20.png" )
	self.button 	= Font.new( "Fonts/FantasqueSansMono-Bold_20.txt", "Fonts/FantasqueSansMono-Bold_20.png" )
	self.small 		= Font.new( "Fonts/ConsolaMono_15.txt", "Fonts/ConsolaMono_15.png" )
	
	self.headerWidth = 24
	self.mainWidth = 8
	self.buttonWidth = 8
	self.smallWidth = 8
end

