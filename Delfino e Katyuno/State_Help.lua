HelpState = Core.class()

function HelpState:init( options )
	self.name = options.name
end

function HelpState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_title.png" ),
		subbackground = Texture.new( "Graphics/UserInterface/shade_frame.png" ),
		buttonback = Texture.new( "Graphics/UserInterface/button_back.png" ),
		buttonforward = Texture.new( "Graphics/UserInterface/button_forward.png" ),
	}
	self.background = Bitmap.new( self.textures.background )
	self.subbackground = Bitmap.new( self.textures.subbackground )
	self.subbackground:setPosition( 10, 10 )
	
	self.images = {}
	
	self.buttons = {
		back = Bitmap.new( self.textures.buttonback ),
		forward = Bitmap.new( self.textures.buttonforward ),
	}
	self.buttons.back:setPosition( 0, SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	self.buttons.forward:setPosition( SCREEN_WIDTH - self.textures.buttonforward:getWidth(), SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	
	
	self.labels = {
		line1 = TextField.new( fontManager.main, languageManager:GetString( "Title" ) ),
		credits1 = TextField.new( fontManager.small, "Rachel J. Morris, " ),
		credits1b = TextField.new( fontManager.small, languageManager:GetString( "EMAIL_ADDRESS" ) ),
		credits1c = TextField.new( fontManager.small, "Moosader.com" ),
		credits2 = TextField.new( fontManager.small, languageManager:GetString( "RachelCredit" ) ),
		credits3 = TextField.new( fontManager.small, languageManager:GetString( "CreditIdoReview1" ) ),
		credits4 = TextField.new( fontManager.small, languageManager:GetString( "CreditIdoReview2" ) ),
		credits5 = TextField.new( fontManager.small, languageManager:GetString( "CreditQA1" ) ),
		credits6 = TextField.new( fontManager.small, languageManager:GetString( "CreditQA2" ) ),
		credits7 = TextField.new( fontManager.small, languageManager:GetString( "CreditAssets" ) ),
		credits8 = TextField.new( fontManager.small, languageManager:GetString( "CreditAssets2" ) ),
		credits9 = TextField.new( fontManager.small, languageManager:GetString( "CreditAssets3" ) ),
		disclaimer1 = TextField.new( fontManager.small, languageManager:GetString( "Disclaimer1" ) ),
		disclaimer2 = TextField.new( fontManager.small, languageManager:GetString( "Disclaimer2" ) ),
		disclaimer3 = TextField.new( fontManager.small, languageManager:GetString( "Disclaimer3" ) ),
		disclaimer4 = TextField.new( fontManager.small, languageManager:GetString( "Disclaimer4" ) ),
	}
	
	local x, y, inc = 15, 30, 15
	for key, value in pairs( self.labels ) do 
		value:setTextColor( 0xffffff ) 
	end
	
	self.labels.line1:setPosition( x, y ); y = y + inc + inc
	
	self.labels.credits1:setPosition( x, y )
	self.labels.credits1c:setPosition( x+200, y ); y = y + inc
	self.labels.credits1b:setPosition( x, y ); y = y + inc
	
	self.labels.credits2:setPosition( x, y ); y = y + inc + inc
	self.labels.credits3:setPosition( x, y ); 
	self.labels.credits5:setPosition( x+200, y ); y = y + inc
	self.labels.credits4:setPosition( x, y ); 
	self.labels.credits6:setPosition( x+200, y ); y = y + inc + inc
	
	self.labels.credits7:setPosition( x, y ); y = y + inc
	self.labels.credits8:setPosition( x, y ); y = y + inc
	self.labels.credits9:setPosition( x, y ); y = y + inc
	
	x = 75
	y = 260
	self.labels.disclaimer1:setPosition( x, y ); y = y + inc
	self.labels.disclaimer2:setPosition( x, y ); y = y + inc
	self.labels.disclaimer3:setPosition( x, y ); y = y + inc
	self.labels.disclaimer4:setPosition( x, y ); y = y + inc
	
	-- credits

	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
	self.background:addEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
end

function HelpState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
	self.background:removeEventListener( Event.MOUSE_UP, self.Event_MouseClick, self )
end

function HelpState:Event_AndroidKey( event )
	if event.keyCode == 301 then
		-- back
		StateManager:StateSetup( "title" )
	end
end

function HelpState:Update()
end

function HelpState:Draw()
	stage:addChild( self.background )
	stage:addChild( self.subbackground )
	
	for key, value in pairs( self.images ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
end

function HelpState:Clear()
	stage:removeChild( self.background )
	stage:removeChild( self.subbackground )
	
	for key, value in pairs( self.images ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.buttons ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:removeChild( value )
	end
end

function HelpState:ReloadLanguage()	
	--self.labels.play:setText( languageManager:GetString( "Play" ) )
end

function HelpState:Event_MouseClick( event )
	if self.buttons.forward:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "howtoplay", false )
	end
end