LevelSelectState = Core.class()

function LevelSelectState:init( options )
	self.name = options.name
	
end

function LevelSelectState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_black.png" ),
		beachbackground = Texture.new( "Graphics/UserInterface/background_beach.png" ),
		buttonbg = Texture.new( "Graphics/UserInterface/button_bg.png" ),
		buttonbgsmall = Texture.new( "Graphics/UserInterface/button_bg_small.png" ),
		buttonback = Texture.new( "Graphics/UserInterface/button_back.png" ),
		buttonhelp = Texture.new( "Graphics/UserInterface/button_help.png" ),
		select_inactive = Texture.new( "Graphics/UserInterface/select_inactive.png" ),
		select_beaten = Texture.new( "Graphics/UserInterface/select_beaten.png" ),
		select_active1 = Texture.new( "Graphics/UserInterface/select_active_1.png" ),
		select_active2 = Texture.new( "Graphics/UserInterface/select_active_2.png" )
	}
	
	self.background = Bitmap.new( self.textures.background )
	self.beachbackground = Bitmap.new( self.textures.beachbackground )
		
	self.buttons = {
		--back = Bitmap.new( self.textures.buttonback ),
		help = Bitmap.new( self.textures.buttonhelp )
	}
	
	self.images = {}	
	
	local width = 90
	local height = 67
	
	local x = 25
	local y = 90
	for i = 1, 5 do
		local adji = i
		self.buttons[ "level" .. adji ] = Bitmap.new( self.textures.select_beaten )
		
		self.buttons[ "level" .. adji ]:setPosition( x, y + 2 )
		
		self.images[ adji ] = Bitmap.new( self.textures.buttonbgsmall )
		self.images[ adji ]:setPosition( 20 + x, 80 + y )
		
		x = x + width
		if x > width * 6 then
			x = width
			y = y + height
		end
	end
	
	--self.buttons.back:setPosition( 0, SCREEN_HEIGHT - self.textures.buttonhelp:getHeight() )	
	self.buttons.help:setPosition( SCREEN_WIDTH - self.textures.buttonhelp:getWidth(), SCREEN_HEIGHT - self.textures.buttonhelp:getHeight() )
	
	self.labels = {
		beach = TextField.new( fontManager.small, languageManager:GetString( "Beach" ) ),
		levelselect = TextField.new( fontManager.header, languageManager:GetString( "LevelSelect" ) )
	}
	
	x = 25
	y = 90
	local row = 1
	
	for i = 1, 5 do
		local label = row .. "-" .. ( (i-1) % 5 + 1 )
		self.labels[ "level" .. i ] = TextField.new( fontManager.small, label )
		self.labels[ "level" .. i ]:setPosition( 23 + x, 93 + y )
		
		if row == 1 then
			self.labels[ "level" .. i ]:setTextColor( 0xffffff )
		elseif row == 2 then
			self.labels[ "level" .. i ]:setTextColor( 0xffffff )
		elseif row == 3 then
			self.labels[ "level" .. i ]:setTextColor( 0xffffff )
		else
			self.labels[ "level" .. i ]:setTextColor( 0xffffff )
		end
		
		x = x + width
		if x > width * 6 then
			x = width
			y = y + height
			row = row + 1
		end
	end
	
	local offsetAmount = 67
	self.labels.beach:setPosition( 10, 0*offsetAmount+20 )
	self.labels.beach:setTextColor( 0x99641f )
	
	local pos = SCREEN_WIDTH / 2 - string.len( languageManager:GetString( "LevelSelect" ) ) * fontManager.headerWidth / 2
	self.labels.levelselect:setPosition( pos, SCREEN_HEIGHT - 50 )
	self.labels.levelselect:setTextColor( 0xffffff )
	
	self.gotoLevel = 0
	
	self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:addEventListener( Event.MOUSE_DOWN, self.Event_ButtonPress, self )
	self.buttons.help:addEventListener( Event.MOUSE_DOWN, self.Event_GotoPlayHelp, self )
	stage:addEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function LevelSelectState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.background:removeEventListener( Event.ENTER_FRAME, self.Update, self )
	self.background:removeEventListener( Event.MOUSE_DOWN, self.Event_ButtonPress, self )
	self.buttons.help:removeEventListener( Event.MOUSE_DOWN, self.Event_GotoPlayHelp, self )
	stage:removeEventListener(Event.KEY_DOWN, self.Event_AndroidKey, self )
end

function LevelSelectState:Event_AndroidKey( event )
	if event.keyCode == 301 then
		-- back
		StateManager:StateSetup( "playmenu" )
	end
end

function LevelSelectState:ReloadLanguage()
	local pos = SCREEN_WIDTH / 2 - string.len( languageManager:GetString( "LevelSelect" ) ) * fontManager.headerWidth / 2
	
	self.labels.beach:setText( languageManager:GetString( "Beach" ) )
	self.labels.levelselect:setText( languageManager:GetString( "LevelSelect" ) )
	
	self.labels.levelselect:setPosition( pos, SCREEN_HEIGHT - 15 )
end

function LevelSelectState:Update()
	
end

function LevelSelectState:Draw()
	stage:addChild( self.background )
	stage:addChild( self.beachbackground )
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.images ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
end

function LevelSelectState:Clear()
	stage:removeChild( self.background )
	stage:removeChild( self.beachbackground )
	
	for key, value in pairs( self.buttons ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.images ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:removeChild( value )
	end
end

function LevelSelectState:Event_GotoPlayMenu( event )
	if self.buttons.back:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "playmenu" )
	end
end

function LevelSelectState:Event_GotoPlayHelp( event )
	if self.buttons.help:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "howtoplay" )
	end
end

function LevelSelectState:Event_ButtonPress( event )
	for i = 1, 5 do
		if self.buttons[ "level" .. i ]:hitTestPoint( event.x, event.y ) then
			StateManager.gotoLevel = i
			--if i % 5 == 0 then
				--StateManager:StateSetup( "cutscene" )
			--else
				StateManager:StateSetup( "storygame" )
			--end
		end
	end
end
