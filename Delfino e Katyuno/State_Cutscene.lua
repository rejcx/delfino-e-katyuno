CutsceneState = Core.class()

function CutsceneState:init( options )
	self.name = options.name	
end

function CutsceneState:Setup()
	self.textures = {
		background = Texture.new( "Graphics/UserInterface/background_black.png" ),
		buttonback = Texture.new( "Graphics/UserInterface/button_back.png" ),
		buttonforward = Texture.new( "Graphics/UserInterface/button_forward.png" ),
	}
	
	self.background = Bitmap.new( self.textures.background )
	
	self.images = {}
	
	if StateManager.gotoLevel == 1 then
		self.images[1] = Bitmap.new( Texture.new( "Graphics/Cutscene/cutscene_1_1.png" ) )
		self.images[2] = Bitmap.new( Texture.new( "Graphics/Cutscene/cutscene_1_2.png" ) )
		self.images[3] = Bitmap.new( Texture.new( "Graphics/Cutscene/cutscene_1_3.png" ) )
		self.endFrame = 4
	end
	
	self.currentFrame = 1
	
	self.buttons = {
		back = Bitmap.new( self.textures.buttonback ),
		forward = Bitmap.new( self.textures.buttonforward )
	}
		
	self.buttons.back:setPosition( 0, SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	self.buttons.forward:setPosition( SCREEN_WIDTH - self.textures.buttonback:getWidth(), SCREEN_HEIGHT - self.textures.buttonback:getHeight() )
	
	self.labels = {}
	
	self.buttons.back:addEventListener( Event.MOUSE_DOWN, self.Event_GotoLevelSelect, self )
	self.buttons.forward:addEventListener( Event.MOUSE_DOWN, self.Event_GotoNext, self )
end

function CutsceneState:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.buttons.back:removeEventListener( Event.MOUSE_DOWN, self.Event_GotoLevelSelect, self )
	self.buttons.forward:removeEventListener( Event.MOUSE_DOWN, self.Event_GotoNext, self )
end

function CutsceneState:ReloadLanguage()
end

function CutsceneState:Update()
end

function CutsceneState:Draw()
	stage:addChild( self.background )
	
	for key, value in pairs( self.buttons ) do
		stage:addChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:addChild( value )
	end
	
	stage:addChild( self.images[ self.currentFrame ] )
end

function CutsceneState:Clear()
	stage:removeChild( self.background )
	
	for key, value in pairs( self.buttons ) do
		stage:removeChild( value )
	end
	
	for key, value in pairs( self.labels ) do
		stage:removeChild( value )
	end
end

function CutsceneState:Event_GotoLevelSelect( event )
	if self.buttons.back:hitTestPoint( event.x, event.y ) then
		StateManager:StateSetup( "levelselect" )
	end
end

function CutsceneState:Event_GotoNext( event )
	if self.buttons.forward:hitTestPoint( event.x, event.y ) then 
		if stage:contains( self.images[ self.currentFrame ] ) then stage:removeChild( self.images[ self.currentFrame ] ) end
		self.currentFrame = self.currentFrame + 1
		
		if ( self.currentFrame == self.endFrame ) then
			StateManager:StateSetup( "storygame" )
		else 
			stage:addChild( self.images[ self.currentFrame ] )
		end
	end
end