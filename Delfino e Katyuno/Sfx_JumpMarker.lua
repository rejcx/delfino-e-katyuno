JumpMarkerSfx = Core.class()

function JumpMarkerSfx:Setup( startX, startY )
	self.timerMax = 12
	self.timer = self.timerMax
	self.destroy = false

	self.bitmap = Bitmap.new( Texture.new( "Graphics/InGame/jumpmarker1.png" ) )
	self.bitmap:setPosition( startX - 25, startY - 25 )
	
	self:Draw()
end

function JumpMarkerSfx:Update()
	if self.destroy == false then
		
		self.timer = self.timer - 1
		
		if self.timer > self.timerMax/3 and self.timer <= self.timerMax*2/3 then
			self.bitmap:setTexture( Texture.new( "Graphics/InGame/jumpmarker2.png" ) )
		
		elseif self.timer > 0 and self.timer <= self.timerMax/3 then
			self.bitmap:setTexture( Texture.new( "Graphics/InGame/jumpmarker3.png" ) )
		
		elseif self.timer <= 0 then
			self:Clear()
			self.destroy = true
			
		end	
	end
end

function JumpMarkerSfx:Clear()
	if stage:contains( self.bitmap ) then stage:removeChild( self.bitmap ) end
end

function JumpMarkerSfx:Draw()
	stage:addChild( self.bitmap )
end