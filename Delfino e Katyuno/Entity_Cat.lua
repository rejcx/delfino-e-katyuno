CatEntity = Core.class( Sprite )

function CatEntity:init()
end

function CatEntity:Setup()
	self.textures = {	
		default = Texture.new( "Graphics/InGame/sprite_cat.png" ),
		falling = Texture.new( "Graphics/InGame/sprite_cat_falling.png" ),
		jumping = Texture.new( "Graphics/InGame/sprite_cat_jumping.png" ),
		}
		
	self.bitmap = Bitmap.new( self.textures.default )
	self.dimensions = { width = 50, height = 50 }
	self.position = { x = 45, y = 375 }
	
	self.velocityY = 0
	self.jumpVelocity = -7
	self.falling = false
	self.secondJump = false
	self.gravity = 0.25
	
	self.dieTimer = 0
	self.dead = false
	
	self.bitmap:addEventListener( Event.ENTER_FRAME, self.Update, self )	
end

function CatEntity:BreakDown()
	-- Remove event binding and remove children
	self:Clear()
	self.bitmap:removeEventListener( Event.ENTER_FRAME, self.Update, self )	
end

function CatEntity:GetCollisionRegion()
	local collidable = {
		position = { x = self.position.x, y = self.position.y },
		dimensions = { width = self.dimensions.width, height = self.dimensions.height }
	}
	
	return collidable
end

function CatEntity:Lose()
	self.bitmap:setTexture( self.textures.falling )
	self.dead = true
	self.gravity = 2
	self.velocityY = self.jumpVelocity
	self.position.y = self.position.y + self.velocityY
end

function CatEntity:IsDead()
	return self.dead == true
end

function CatEntity:Update()
	if self.falling == true then
		self.position.y = self.position.y + self.velocityY
		self.velocityY = self.velocityY + self.gravity
	elseif self.dead == true then
		self.position.x = self.position.x - 0.5
		self.position.y = self.position.y + self.velocityY
		self.velocityY = self.velocityY + (self.gravity/2)
	end

	if self.position.y < -self.dimensions.height/2 then
		self.position.y = -self.dimensions.height/2
	end
	
	self.bitmap:setPosition( self.position.x, self.position.y )
end

function CatEntity:IsFalling( dolphin )
	if self.dead == false and self.position.y + self.dimensions.height > dolphin.position.y then
		self.position.y = dolphin.position.y - self.dimensions.height
	end
	
	if self.dead == true then
		self.bitmap:setTexture( self.textures.falling )
	
	else
		if dolphin.position.y <= self.position.y + self.dimensions.height then
			self.falling = false
			self.velocityY = 0
			self.secondJump = false
			self.bitmap:setTexture( self.textures.default )
		else
			self.falling = true
		end
	end
end

function CatEntity:Draw()
	stage:addChild( self.bitmap )
end

function CatEntity:Clear()
	if stage:contains( self.bitmap ) then stage:removeChild( self.bitmap ) end
end

function CatEntity:Jump()
	if self.dead == false then
		if self.falling == false then
			self.velocityY = self.jumpVelocity
			self.falling = true
			self.position.y = self.position.y + self.velocityY
			self.bitmap:setTexture( self.textures.jumping )
		else
			self:SecondJump()
		end
	end
end

function CatEntity:SecondJump()
	if self.falling == true and self.secondJump == false then
		self.velocityY = self.jumpVelocity
		self.secondJump = true
		self.position.y = self.position.y + self.velocityY
		self.bitmap:setTexture( self.textures.jumping )
	end
end