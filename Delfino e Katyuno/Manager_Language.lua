LanguageManager = Core.class()

function LanguageManager:init( options )
	self.languages = { "en", "ido", "eo" }
	self.currentLanguage = 1
	self:SetLanguage( options.language )
	
	self.strings = {
		en = {},
		ido = {},
		eo = {},
	}
	
	self.strings.en.Title = { text = "Dolphin & Kitten" }
	self.strings.ido.Title = { text = "Delfino e Katyuno" }
	self.strings.eo.Title = { text = "Delfeno kaj Katido" }
	
	self.strings.en.Language = { text = "English" }
	self.strings.ido.Language = { text = "Ido" }
	self.strings.eo.Language = { text = "Eo" }
	
	self.strings.en.Play = { text = "Play" }
	self.strings.ido.Play = { text = "Ludar" }
	self.strings.eo.Play = { text = "Ludi" }
	
	self.strings.en.Help = { text = "Help" }
	self.strings.ido.Help = { text = "Helpar" }
	self.strings.eo.Help = { text = "Helpi" }
	
	self.strings.en.Options = { text = "Options" }
	self.strings.ido.Options = { text = "Selekti" }
	self.strings.eo.Options = { text = "Opcioj" }
	
	self.strings.en.Story = { text = "Story" }
	self.strings.ido.Story = { text = "Rakonto" }
	self.strings.eo.Story = { text = "Rakonto" }
	
	self.strings.en.Challenge = { text = "Challenge" }
	self.strings.ido.Challenge = { text = "Desfacilludo" }
	self.strings.eo.Challenge = { text = "Delfiludo" }
	
	self.strings.en.CustomLevels = { text = "Custom Levels" }
	self.strings.ido.CustomLevels = { text = "Mapi di Uzanto" }
	self.strings.eo.CustomLevels = { text = "Mapoj de Uzanto" }
	
	self.strings.en.Editor = { text = "Editor" }
	self.strings.ido.Editor = { text = "Kreilo di Mapi" }
	self.strings.eo.Editor = { text = "Mapkreilo" }
	
	self.strings.en.Battle = { text = "Battle" }
	self.strings.ido.Battle = { text = "Batalio" }
	self.strings.eo.Battle = { text = "Batalo" }
	
	self.strings.en.Beach = { text = "Beach" }
	self.strings.ido.Beach = { text = "Plajo" }
	self.strings.eo.Beach = { text = "Plaĝo" }
	
	self.strings.en.Sky = { text = "Sky" }
	self.strings.ido.Sky = { text = "Cielo" }
	self.strings.eo.Sky = { text = "Ĉielo" }
	
	self.strings.en.Underwater = { text = "Underwater" }
	self.strings.ido.Underwater = { text = "Submaro" }
	self.strings.eo.Underwater = { text = "Submaro" }
	
	self.strings.en.Antarctic = { text = "Antarctic" }
	self.strings.ido.Antarctic = { text = "Antarktiko" }
	self.strings.eo.Antarctic = { text = "Antarkto" }
	
	self.strings.en.LevelSelect = { text = "Level Select" }
	self.strings.ido.LevelSelect = { text = "Selektez Mapo" }
	self.strings.eo.LevelSelect = { text = "Elekti Mapon" }
	
	self.strings.en.ChooseATile = { text = "Choose a Brush" }
	self.strings.ido.ChooseATile = { text = "Elekti Brosilon" }
	
	self.strings.en.Filename = { text = "Filename" }
	self.strings.ido.Filename = { text = "Dosiernomo" }
	self.strings.eo.Filename = { text = "Dosiernomo" }
	
	self.strings.en.Save = { text = "Save" }
	self.strings.ido.Save = { text = "Konservar" }
	self.strings.eo.Save = { text = "Konservi" }
	
	self.strings.en.Load = { text = "Load" }
	self.strings.ido.Load = { text = "Charjar" }
	self.strings.eo.Load = { text = "Ŝargi" }
	
	self.strings.en.GameType = { text = "Game Type" }
	self.strings.ido.GameType = { text = "Ludo Tipo" }
	self.strings.eo.GameType = { text = "Ludtipo" }
	
	self.strings.en.GameTypeHop = { text = "Hop" }
	self.strings.ido.GameTypeHop = { text = "Salto" }
	self.strings.eo.GameTypeHop = { text = "Hopo" }
	
	self.strings.en.GameTypeFly = { text = "Fly" }
	self.strings.ido.GameTypeFly = { text = "Flugo" }
	self.strings.eo.GameTypeFly = { text = "Flugo" }
	
	self.strings.en.GameTypeSwim = { text = "Swim" }
	self.strings.ido.GameTypeSwim = { text = "Nato" }
	self.strings.eo.GameTypeSwim = { text = "Naĝo" }
	
	self.strings.en.ClearLevel = { text = "Clear Level" }
	self.strings.ido.ClearLevel = { text = "Efacez Mapo" }
	self.strings.eo.ClearLevel = { text = "Klariĝi Mapon" }
	
	self.strings.en.TestLevel = { text = "Test Level" }
	self.strings.ido.TestLevel = { text = "Probez Mapo" }
	self.strings.eo.TestLevel = { text = "Provi Mapon" }
	
	self.strings.en.TileCount = { text = "Tile Count" }
	self.strings.ido.TileCount = { text = "Quanto de Teguli" }
	self.strings.eo.TileCount = { text = "Sumo de Teguloj" }
	
	self.strings.en.BackToMenu = { text = "Exit Level Editor" }
	self.strings.ido.BackToMenu = { text = "Forirez Kreilo " }
	self.strings.eo.BackToMenu = { text = "Eliri Kreilo " }
	
	self.strings.en.EnterLevelFilename = { text = "Enter the level filename" }
	self.strings.ido.EnterLevelFilename = { text = "Skribez la nomo di la dosiero" }
	self.strings.eo.EnterLevelFilename = { text = "Skribu la nomon por la dosiero" }
	
	self.strings.en.DefaultMapName = { text = "default" }
	self.strings.ido.DefaultMapName = { text = "mea" }
	self.strings.eo.DefaultMapName = { text = "mia" }
	
	self.strings.en.OK = { text = "OK" }
	self.strings.ido.OK = { text = "Konfirmar" }
	self.strings.eo.OK = { text = "Konfirmi" }
	
	self.strings.en.Cancel = { text = "Cancel" }
	self.strings.ido.Cancel = { text = "Abrogar" }
	self.strings.eo.Cancel = { text = "Nuligi" }
	
	self.strings.en.ErrorOpeningFile = { text = "Error opening file" }
	self.strings.ido.ErrorOpeningFile = { text = "Eroro kande apertis dosiero" }
	self.strings.eo.ErrorOpeningFile = { text = "Eroro kiam malfermis dosieron" }
	
	self.strings.en.SavedLevel = { text = "Saved level" }
	self.strings.ido.SavedLevel = { text = "Konservis mapo" }
	self.strings.eo.SavedLevel = { text = "Konservis la mapon" }
	
	self.strings.en.LoadedLevel = { text = "Loaded level" }
	self.strings.ido.LoadedLevel = { text = "Charjis mapo" }
	self.strings.eo.LoadedLevel = { text = "Ŝargis la mapon" }
	
	self.strings.en.ClearedLevel = { text = "Cleared level" }
	self.strings.ido.ClearedLevel = { text = "Obliteris mapo" }
	self.strings.eo.ClearedLevel = { text = "Klariĝis la mapon" }
	
	self.strings.en.IMessedUp = { text = "I messed up :|" }
	self.strings.ido.IMessedUp = { text = "Mi facas eroron :|" }
	self.strings.eo.IMessedUp = { text = "Mi faris eraron :|" }
	
	self.strings.en.UnableToOpenLevel = { text = "Unable to open level" }
	self.strings.ido.UnableToOpenLevel = { text = "Ne povis apertar la mapo" }
	self.strings.eo.UnableToOpenLevel = { text = "Ne povis ŝargi la mapon!" }
	
	self.strings.en.PleaseContact = { text = "Please contact the developer at" }
	self.strings.ido.PleaseContact = { text = "Bonvole, e-postez la programisto che" }
	self.strings.eo.PleaseContact = { text = "Bonvolu, retpoŝtu al la programisto" }
	
	self.strings.en.Damnit = { text = "Damnit!" }
	self.strings.ido.Damnit = { text = "Damne!" }
	self.strings.eo.Damnit = { text = "Damne!" }
	
	self.strings.en.RetryLevel = { text = "Retry Level" }
	self.strings.ido.RetryLevel = { text = "Rekomencez Mapo" }
	self.strings.eo.RetryLevel = { text = "Rekomenci Mapon" }
	
	self.strings.en.LevelSelect = { text = "Level Select" }
	self.strings.ido.LevelSelect = { text = "Selektez Mapo" }
	self.strings.eo.LevelSelect = { text = "Elekti Mapon" }
	
	self.strings.en.NextLevel = { text = "Next Level" }
	self.strings.ido.NextLevel = { text = "Venanta Mapo" }
	self.strings.eo.NextLevel = { text = "Iri al Sekvonta Mapo" }
	
	self.strings.en.YouLose = { text = "Too Bad!" }
	self.strings.ido.YouLose = {text = "Falio!" }
	self.strings.eo.YouLose = {text = "Malsukcesego!" }
	
	self.strings.en.YouWin = { text = "Success!" }
	self.strings.ido.YouWin = {text = "Suceso!" }
	self.strings.eo.YouWin = {text = "Sukcesego!" }
	
	self.strings.en.BackToEditor = { text = "Back to Editor" }
	self.strings.ido.BackToEditor = { text = "Rivenar ad mapo kreilo" }
	self.strings.eo.BackToEditor = { text = "Reveni al la mapkreilo" }
	
	self.strings.en.PausedGame = { text = "Game Paused" }
	self.strings.ido.PausedGame = { text = "Pauzis la Ludo" }
	self.strings.eo.PausedGame = { text = "Paŭzis la Ludon" }
	
	self.strings.en.Level = { text = "Level" }
	self.strings.ido.Level = { text = "Mapo" }
	self.strings.eo.Level = { text = "Mapo" }
	
	self.strings.en.HowToPlay = { text = "How to Play" }
	self.strings.ido.HowToPlay = { text = "Quale Ludar" }
	self.strings.eo.HowToPlay = { text = "Kiel Ludi" }
	
	self.strings.en.Credits = { text = "Credits" }
	self.strings.ido.Credits = { text = "Krenati" }
	self.strings.eo.Credits = { text = "Kreditoj" }
	
	self.strings.en.RachelCredit = { text = "Programming, Art, Temporary Music, Quality Testing" }
	self.strings.ido.RachelCredit = { text = "Programado, Arto, Muziko, Qualeso Probar" }
	self.strings.eo.RachelCredit = { text = "Programado, Desegnado, Muziko, Testado" }
	
	self.strings.en.CreditIdoReview1 = { text = "??" }
	self.strings.ido.CreditIdoReview1 = { text = "??" }
	self.strings.eo.CreditIdoReview1 = { text = "??" }
	
	self.strings.en.CreditIdoReview2 = { text = "Ido Review" }
	self.strings.ido.CreditIdoReview2 = { text = "Revuis Ido" }
	self.strings.eo.CreditIdoReview2 = { text = "Esperanto Recenzo" }
	
	self.strings.en.CreditQA1 = { text = "??" }
	self.strings.ido.CreditQA1 = { text = "??" }
	self.strings.eo.CreditQA1 = { text = "??" }
	
	self.strings.en.CreditQA2 = { text = "Quality Testing" }
	self.strings.ido.CreditQA2 = { text = "Qualeso Probar" }
	self.strings.eo.CreditQA2 = { text = "Qualeso Probar" }
	
	self.strings.en.CreditAssets = { text = "Fonts: Not Courier Sans," }
	self.strings.ido.CreditAssets = { text = "Tipari: Not Courier Sans," }
	self.strings.eo.CreditAssets = { text = "Tiparoj: Not Courier Sans," }
	
	self.strings.en.CreditAssets2 = { text = "Fantasque Sans Mono, Consola Mono" }
	self.strings.ido.CreditAssets2 = { text = "Fantasque Sans Mono, Consola Mono" }
	self.strings.eo.CreditAssets2 = { text = "Fantasque Sans Mono, Consola Mono" }
	
	self.strings.en.CreditAssets3 = { text = "Temporary Sound: BFXR" }
	self.strings.ido.CreditAssets3 = { text = "Tempala Sono: BFXR" }
	self.strings.eo.CreditAssets3 = { text = "Tempala Sono: BFXR" }
	
	self.strings.en.Disclaimer1 = { text = "This game is a work in progress." }
	self.strings.ido.Disclaimer1 = { text = "Ica ludo estas nekompleta." }
	self.strings.eo.Disclaimer1 = { text = "Ĉi tiu ludo estas nekompleta." }
	
	self.strings.en.Disclaimer2 = { text = "Graphics, gameplay, quality," }
	self.strings.ido.Disclaimer2 = { text = "Grafiki, ludludi, qualeso," }
	self.strings.eo.Disclaimer2 = { text = "Arto, ludludo, Kvalito," }
	
	self.strings.en.Disclaimer3 = { text = "and other content may" }
	self.strings.ido.Disclaimer3 = { text = "ed altra kontentajo povus" }
	self.strings.eo.Disclaimer3 = { text = "kaj aliaj aĵoj povos" }
	
	self.strings.en.Disclaimer4 = { text = "change in the final version." }
	self.strings.ido.Disclaimer4 = { text = "chanjar en la fina versiono." }
	self.strings.eo.Disclaimer4 = { text = "ŝanĝi en la fina versiono." }
	
	self.strings.en.Help1 = { text = "Meet Delfino & Katyuno." }
	self.strings.ido.Help1 = { text = "Renkentrez Delfino e Katyuno." }
	self.strings.eo.Help1 = { text = "Renkontu Delfeno kaj Katido." }
	
	self.strings.en.Help2 = { text = "You will control their jumps," }
	self.strings.ido.Help2 = { text = "---" }
	self.strings.eo.Help2 = { text = "Vi direktos iliajn saltojn." }
	
	self.strings.en.Help3 = { text = "avoid obstacles." }
	self.strings.ido.Help3 = { text = "---" }
	self.strings.eo.Help3 = { text = "Evitu danĝerojn." }
	
	self.strings.en.Help4 = { text = "---." }
	self.strings.ido.Help4 = { text = "---" }
	self.strings.eo.Help4 = { text = "---" }
	
	self.strings.en.Help5 = { text = "When Delfino and Katyuno are at rest," }
	self.strings.ido.Help5 = { text = "" }
	self.strings.eo.Help5 = { text = "Kiam Delfeno kaj Katido senfunkcias," }
	
	self.strings.en.Help6 = { text = "and you tap either Katyuno or Delfino," }
	self.strings.ido.Help6 = { text = "---" }
	self.strings.eo.Help6 = { text = "kaj vi frapetas Katido aŭ Delfeno," }
	
	self.strings.en.Help7 = { text = "Katyuno will be tossed into the air." }
	self.strings.ido.Help7 = { text = "" }
	self.strings.eo.Help7 = { text = "Katido saltos!" }
	
	self.strings.en.Help8 = { text = "While Katyuno is in the air," }
	self.strings.ido.Help8 = { text = "" }
	self.strings.eo.Help8 = { text = "Kiam Katyido estas en la aero," }
	
	self.strings.en.Help9 = { text = "tap her again to double jump." }
	self.strings.ido.Help9 = { text = "---" }
	self.strings.eo.Help9 = { text = "refrapetu ŝin por duobla-salto" }
	
	self.strings.en.Help10 = { text = "While Katyuno is in the air," }
	self.strings.ido.Help10 = { text = "" }
	self.strings.eo.Help10 = { text = "Kiam Katido estas en la aero," }
	
	self.strings.en.Help11 = { text = "tap Delfino to make him jump." }
	self.strings.ido.Help11 = { text = "" }
	self.strings.eo.Help11 = { text = "frapetu Delfeno por delfensalti" }
	
	self.strings.en.CollectThese = { text = "Collect These:" }
	self.strings.ido.CollectThese = { text = "Kolektez Ici:" }
	self.strings.eo.CollectThese = { text = "Kolektu ĉi tiujn:" }
	
	self.strings.en.AvoidThese = { text = "Avoid These:" }
	self.strings.ido.AvoidThese = { text = "Evitez Ici:" }
	self.strings.eo.AvoidThese = { text = "Evitu ĉi tiujn:" }
	
	self.strings.en.WelcomeToGame = { text = "Welcome to Delfino & Katyuno!" }
	self.strings.ido.WelcomeToGame = { text = "Bonveno ad 'Delfino e Katyuno'!" }
	self.strings.eo.WelcomeToGame = { text = "Bonvenon al 'Delfeno kaj Katido'!" }
	
	self.strings.en.TapCatToJump = { text = "Tap Katyuno to jump once" }
	self.strings.ido.TapCatToJump = { text = "Frapetez Katyuno ad saltar unfoye" }
	self.strings.eo.TapCatToJump = { text = "Frapetu Katido por salti unu fojon" }
	
	self.strings.en.DoubleJump = { text = "Tap Katyuno while in the air to double-jump" }
	self.strings.ido.DoubleJump = { text = "Frapetez Katyuno kande elu saltas ad duople saltar" }
	self.strings.eo.DoubleJump = { text = "Frapetu Katido kiam ŝi saltas por duoblsalti" }
	
	self.strings.en.DolphinJump = { text = "When Katyuno is in the air, tap Delfino to jump" }
	self.strings.ido.DolphinJump = { text = "Kande Katyuno esas en la areo, frapetez Delfino ad saltar" }
	self.strings.eo.DolphinJump = { text = "Kiam Katido flugas, frapetu Delfeno por salti lin." }
	
	self.strings.en.EndOfLevel = { text = "Here's the end of the level!" }
	self.strings.ido.EndOfLevel = { text = "Hike venas la fino di la mapo!" }
	self.strings.eo.EndOfLevel = { text = "Jen, la fino de la mapo!" }
	
	self.strings.en.CollectHearts = { text = "Collect Hearts for Extra Life" }
	self.strings.ido.CollectHearts = { text = "Kolektez Kordii por Recevas Extra Vivo" }
	self.strings.eo.CollectHearts = { text = "Kolektu korojn por ricevi ekstrajn vivojn" }
	
	self.strings.en.AvoidObstacles = { text = "Avoid Obstacles and Predators" }
	self.strings.ido.AvoidObstacles = { text = "Evitez Obstakli e Danjerozi animali" }
	self.strings.eo.AvoidObstacles = { text = "Evlitu obstaklojn kaj danĝerajn bestojn" }
	
	self.strings.en.CollectTrinkets = { text = "Collect Trinkets" }
	self.strings.ido.CollectTrinkets = { text = "Kolektez Blua Kozi" }
	self.strings.eo.CollectTrinkets = { text = "Kolektu brelokojn" }
	
	self.strings.en.CatFirst = { text = "Kitten must jump before Dolphin" }
	self.strings.ido.CatFirst = { text = "Katyuno devas saltar ante Delfino" }
	self.strings.eo.CatFirst = { text = "Katido devas salti antaŭ Delfino povas" }
	
	self.strings.en.DoingGood = { text = "Doing good?" }
	self.strings.ido.DoingGood = { text = "Facado bone?" }
	self.strings.eo.DoingGood = { text = "Facado bone?" }
	
	
	-- General stuff
	
	self.strings.en.EMAIL_ADDRESS = { text = "Rachel@Moosader.com" }
	self.strings.ido.EMAIL_ADDRESS = { text = "Rachel@Moosader.com" }
	self.strings.eo.EMAIL_ADDRESS = { text = "Rachel@Moosader.com" }
	
	self.strings.en.VERSION = { text = "v0.2" }
	self.strings.ido.VERSION = { text = "v0.2" }
	self.strings.eo.VERSION = { text = "v0.2" }
		
		
end

function LanguageManager:GetString( key )
	local currentLanguage = self.languages[ self.currentLanguage ]
	return self.strings[ currentLanguage ][ key ]["text"]
end

function LanguageManager:GetPosition( key )
	local currentLanguage = self.languages[ self.currentLanguage ]
	return self.strings[ currentLanguage ][ key ]["posX"]
end

function LanguageManager:SetLanguage( language )
	for key, value in pairs( self.languages ) do
		if value == language then
			self.currentLanguage = key
		end
	end
end

function LanguageManager:GetLanguage()
	return self.languages[ self.currentLanguage ]
end


